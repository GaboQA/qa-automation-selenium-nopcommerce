import random
import time
import unittest
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support import expected_conditions as EC, select
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import TimeoutException
from Functions.Home_page import Functions_Home_Page
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.action_chains import ActionChains
from Functions.General_functions import General_Functions

class loginPage(unittest.TestCase):

    def setUp(self):
        self.driver = webdriver.Chrome(executable_path='C:\Dchroom\R\chromedriver.exe')

    def test_Succesfull(self):
        driver = self.driver
        driver.get('https://demo.nopcommerce.com/register')
        driver.maximize_window()
        G = General_Functions(driver)
        G.selectRandom('n/a','//*[@class="female"  or @class="male"]','checkbox')
        time.sleep(2)
        driver.find_element(By.XPATH,'//*[@name="FirstName"]').send_keys('Gabriel')
        driver.find_element(By.XPATH,'//*[@id="LastName"]').send_keys('Quesada')
        G.selectRandom('//*[@name="DateOfBirthDay"]', 'value', 'Select')
        G.selectRandom('//*[@name="DateOfBirthMonth"]', 'value', 'Select')
        G.selectRandom('//*[@name="DateOfBirthYear"]', 'value', 'Select')
        rd = random.randint(1,1000)
        driver.find_element(By.XPATH,'//*[@name="Email"]').send_keys('GabrielQA'+str(rd)+'@yopmail.com')
        G.selectRandom('n/a', '//*[@name="Newsletter" and @type="checkbox"]', 'checkbox')
        driver.find_element(By.XPATH,'//*[@id="Password"]').send_keys('Gabriel123.')
        driver.find_element(By.XPATH,'//*[@id="ConfirmPassword"]').send_keys('Gabriel123.')
        driver.find_element(By.XPATH,'//*[@id="register-button"]').click()
        time.sleep(5)
        driver.find_element(By.XPATH,'//*[text()="Your registration completed"]')
        print('Complete')


    def test_WithSpaces(self):
        driver = self.driver
        driver.get('https://demo.nopcommerce.com/register')
        driver.maximize_window()
        G = General_Functions(driver)
        G.selectRandom('n/a','//*[@class="female"  or @class="male"]','checkbox')
        time.sleep(2)
        driver.find_element(By.XPATH,'//*[@name="FirstName"]').send_keys('  ')
        driver.find_element(By.XPATH,'//*[@id="LastName"]').send_keys('  ')
        G.selectRandom('//*[@name="DateOfBirthDay"]', 'value', 'Select')
        G.selectRandom('//*[@name="DateOfBirthMonth"]', 'value', 'Select')
        G.selectRandom('//*[@name="DateOfBirthYear"]', 'value', 'Select')
        rd = random.randint(1,1000)
        driver.find_element(By.XPATH,'//*[@name="Email"]').send_keys('GabrielQA'+str(rd)+'@yopmail.com')
        G.selectRandom('n/a', '//*[@name="Newsletter" and @type="checkbox"]', 'checkbox')
        driver.find_element(By.XPATH,'//*[@id="Password"]').send_keys('Gabriel123.')
        driver.find_element(By.XPATH,'//*[@id="ConfirmPassword"]').send_keys('Gabriel123.')
        driver.find_element(By.XPATH,'//*[@id="register-button"]').click()
        time.sleep(5)
        driver.find_element(By.XPATH,'//*[text()="Last name is required."]')
        driver.find_element(By.XPATH,'//*[text()="First name is required."]')


    def test_WithoutChecks(self):
        driver = self.driver
        driver.get('https://demo.nopcommerce.com/register')
        driver.maximize_window()
        G = General_Functions(driver)
        G.selectRandom('n/a','//*[@class="female"  or @class="male"]','checkbox')
        time.sleep(2)
        driver.find_element(By.XPATH,'//*[@name="FirstName"]').send_keys('Gab')
        driver.find_element(By.XPATH,'//*[@id="LastName"]').send_keys('QA')
        G.selectRandom('//*[@name="DateOfBirthDay"]', 'value', 'Select')
        G.selectRandom('//*[@name="DateOfBirthMonth"]', 'value', 'Select')
        G.selectRandom('//*[@name="DateOfBirthYear"]', 'value', 'Select')
        rd = random.randint(1,1000)
        driver.find_element(By.XPATH,'//*[@name="Email"]').send_keys('GabrielQA'+str(rd)+'@yopmail.com')
        driver.find_element(By.XPATH,'//*[@id="Password"]').send_keys('Gabriel123.')
        driver.find_element(By.XPATH,'//*[@id="ConfirmPassword"]').send_keys('Gabriel123.')
        driver.find_element(By.XPATH,'//*[@id="register-button"]').click()
        time.sleep(5)
        driver.find_element(By.XPATH,'//*[text()="Your registration completed"]')



    def test_PasswordNotTheSame(self):
        driver = self.driver
        driver.get('https://demo.nopcommerce.com/register')
        driver.maximize_window()
        G = General_Functions(driver)
        driver.find_element(By.XPATH,'//*[@id="Password"]').send_keys('Gabriel123.')
        driver.find_element(By.XPATH,'//*[@id="ConfirmPassword"]').send_keys('Gabriel1233', Keys.ENTER)
        time.sleep(2)
        driver.find_element(By.XPATH,'//*[text()="The password and confirmation password do not match."]')



    def test_PasswordWitoutRequirement(self):
        driver = self.driver
        driver.get('https://demo.nopcommerce.com/register')
        driver.maximize_window()
        G = General_Functions(driver)
        driver.find_element(By.XPATH,'//*[@id="Password"]').send_keys('123')
        driver.find_element(By.XPATH,'//*[@id="ConfirmPassword"]').send_keys('123')
        driver.find_element(By.XPATH,'//*[@id="register-button"]')
        time.sleep(5)
        driver.find_element(By.XPATH,'//*[text()="Password must meet the following rules: "]')
        driver.find_element(By.XPATH,'//*[text()="must have at least 6 characters"]')



    def tearDown(self):
        self.driver.close()

if __name__ == '__main__':
    unittest.main()