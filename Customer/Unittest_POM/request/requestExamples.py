import time
import unittest
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support import expected_conditions as EC, select
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import TimeoutException


page = 'https://demo.nopcommerce.com/apple-macbook-pro-13-inch'
import json
import requests


class home_page(unittest.TestCase):


    def setUp(self):
        self.driver = webdriver.Chrome(executable_path='C:\Dchroom\R\chromedriver.exe')

    def test_get(self):
        url = 'https://jsonplaceholder.typicode.com/posts'
        response = requests.get(url)
        data = response.json()
        for datasd in data:
            print(datasd['title'])

    def test_post(self):
        url = 'https://jsonplaceholder.typicode.com/posts'

        data = {
            'title': 'Ejemplo de título',
            'body': 'Ejemplo de contenido',
            'userId': 1
        }

        response = requests.post(url, json=data)
        result = response.json()

        print(result)



    def test_postwithToken(self):
        url = 'https://demo.nopcommerce.com/addproducttocart/details/4/1'

        data = {
            'addtocart_4.EnteredQuantity': 2,
            '__RequestVerificationToken': 'CfDJ8Jtw2KziYYJGnXbyZNE5L9SYVkFwRDQuxJMHLCy2Ijs1YgJcs8LjCg9gnHm5Nc-9PUIenp0RHUFWCu4TIQNVcZq0gPPAyeTrZpABBKPNOOlHXAbM5Iqgvr_Qt_8taLZu4Pg6sPpyPF8nnZWWUCg9nnIuJXEGaeeIg89x104px_CAzkJoWO_KVchdV5GUGOyvLg'
        }
        response = requests.post(url, json=data)
        result = response.json()

        print(result)

    def tearDown(self):
        self.driver.close()


if __name__ == '__main__':
    unittest.main()
