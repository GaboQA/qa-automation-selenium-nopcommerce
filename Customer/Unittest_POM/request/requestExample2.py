import time
from selenium import webdriver
from selenium.webdriver.common.by import By
import requests
import urllib.parse



class Fcuntions_request():


    def __init__(self,driver):
        self.driver = driver

    def GET(self,Token,URL):
        # Configurar las opciones de descarga
        options = webdriver.ChromeOptions()
        options.add_experimental_option("prefs", {
            "download.default_directory": "C:\\Users\\GabrielPC\\OneDrive\\Documentos\\qa-automation-selenium-nopcommerce\\files",
            "download.prompt_for_download": False,
            "download.directory_upgrade": True,
            "safebrowsing.enabled": True
        })

        # Iniciar el navegador con las opciones de descarga configuradas
        driver = webdriver.Chrome(executable_path='C:\Dchroom\R\chromedriver.exe', options=options)

        # Localizar el elemento del enlace de descarga y hacer clic en él

        driver.get(URL)
        driver.find_element(By.XPATH,"/html/body/p[2]/a/img").click()
        time.sleep(5)
        return

    #First request to complete the others, this is required
    def RequestBearerToken(self):
        url = 'https://URL/api/v1/login/access-token'
        data = {
            'username': 'super.admin',
            'password': '$TecPass05'
        }
        try:
            response = requests.post(url, json=data, verify=False)
            response.raise_for_status()  # Will generate a exception
            result = response.json()
            return result['access_token']

        except requests.exceptions.RequestException as e:
            print(f"Error en la solicitud: {e}")

    def GetAssetIT(self):
        url = 'https://URL/api/v1/assets_inventory/it'
        try:
            token = Fcuntions_request.RequestBearerToken(self)
            headers = {
                'Authorization': f'Bearer {token}'
            }

            response = requests.get(url, headers=headers, verify=False)
            response.raise_for_status()
            result = response.json()

        except requests.exceptions.RequestException as e:
            print(f"Error en la solicitud: {e}")

