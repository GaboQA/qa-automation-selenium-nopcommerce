import time
import unittest
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support import expected_conditions as EC, select
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import TimeoutException
from Functions.Home_page import Functions_Home_Page
from selenium.webdriver.common.action_chains import ActionChains

page = 'https://demo.nopcommerce.com/'

class home_page(unittest.TestCase):

    def setUp(self):
        self.driver = webdriver.Chrome(executable_path='C:\Dchroom\R\chromedriver.exe')


    def test_home_header_sect1(self):
        driver = self.driver
        f= Functions_Home_Page(driver)
        driver.get(page)
        driver.maximize_window()
        imagen = driver.find_element(By.XPATH,'//*[@class="header-upper"]')
        f.Time(3)
        f.Try_except(imagen)


    def test_home_header_sect2(self):
        driver = self.driver
        f= Functions_Home_Page(driver)
        driver.get(page)
        texto = driver.find_element(By.XPATH,'//*[text()="nopCommerce"]')
        assert texto.text == "nopCommerce"


    def test_home_header_sect3(self):
        driver = self.driver
        driver.get(page)
        driver.maximize_window()
        texto = driver.find_element_by_xpath('//*[text()="Community poll"]')
        assert texto.text == 'Community poll'



    def test_home_sect1(self):
        driver = self.driver
        f = Functions_Home_Page(driver)
        driver.get(page)
        driver.maximize_window()
        #driver.switch_to.frame(driver.find_element(By.XPATH, '/html/body/main/div/iframe'))
        Img = driver.find_element(By.XPATH, '/html/body/div[6]/div[1]/div[2]/div[1]/a/img')
        f.Try_except(Img)
        #Validamo monedas disponibles
        f.Selects('//*[@id="customerCurrency"]', 'option')
        #Validamos redireccionamientos
        f.Redirects('/html/body/div[6]/div[1]/div[1]/div[2]/div[1]/ul/li[1]/a','/html/body/div[6]/div[3]/div/div/div/div[1]/h1')
        f.Redirects('/html/body/div[6]/div[1]/div[1]/div[2]/div[1]/ul/li[2]/a','/html/body/div[6]/div[3]/div/div/div/div[1]/h1')
        f.Redirects('/html/body/div[6]/div[1]/div[1]/div[2]/div[1]/ul/li[3]/a','/html/body/div[6]/div[3]/div/div/div/div[1]/h1')
        f.Redirects('/html/body/div[6]/div[1]/div[1]/div[2]/div[1]/ul/li[4]/a','/html/body/div[6]/div[3]/div/div/div/div[1]/h1')


    def test_home_sect2(self):
        driver = self.driver
        f = Functions_Home_Page(driver)
        driver.get(page)
        driver.maximize_window()
        #driver.switch_to.frame(driver.find_element(By.XPATH, '/html/body/main/div/iframe'))
        #Validamos el tipo de imagen exista
        a = driver.find_element_by_xpath('/html/body/div[6]/div[1]/div[2]/div[1]/a/img')
        assert a.get_attribute("src") == 'https://demo.nopcommerce.com/Themes/DefaultClean/Content/images/logo.png'
        #Validamos que el search este funcionando
        f.Search('//*[@id="small-searchterms"]','//*[@id="small-search-box-form"]/button')
        #Validamos que encuentren productos disponibles e imprimimos cada uno.


    def test_home_sec3(self):
        driver = self.driver
        driver = self.driver
        f = Functions_Home_Page(driver)
        driver.get(page)
        driver.maximize_window()
        #driver.switch_to.frame(driver.find_element(By.XPATH, '/html/body/main/div/iframe'))
        # Validamos el redireccionamiento del menu de elementos
        f.Hover("/html/body/div[6]/div[2]/ul[1]/li[1]/a")
        f.Hover("/html/body/div[6]/div[2]/ul[1]/li[2]/a")
        f.Hover("/html/body/div[6]/div[2]/ul[1]/li[3]/a")
        f.Hover("/html/body/div[6]/div[2]/ul[1]/li[4]/a")
        f.Hover("/html/body/div[6]/div[2]/ul[1]/li[5]/a")
        f.Hover("/html/body/div[6]/div[2]/ul[1]/li[6]/a")
        f.Hover("/html/body/div[6]/div[2]/ul[1]/li[7]/a")


    def test_home_sec4(self):
        driver = self.driver
        driver.get(page)
        driver.maximize_window()
        #driver.switch_to.frame(driver.find_element(By.XPATH, '/html/body/main/div/iframe'))
        # Validamos que existan las imagenes en el carrucel
        for i in range(2):
            time.sleep(3)
            a = driver.find_element(By.XPATH,'//*[@id="nivo-slider"]/a[1]/img')
            b = driver.find_element(By.XPATH,'//*[@id="nivo-slider"]/a[2]/img')
            if (i <= 1):
                assert a.get_attribute("src") == 'https://demo.nopcommerce.com/images/thumbs/0000088_banner_01.webp'
            else:
                assert b.get_atributte("src") == 'https://demo.nopcommerce.com/images/thumbs/0000088_banner_01.webp'



    def tearDown(self):
        self.driver.close()

if __name__ == '__main__':
    unittest.main()