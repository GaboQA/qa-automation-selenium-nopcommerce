import time
import unittest
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support import expected_conditions as EC, select
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import TimeoutException
from Functions.Home_page import Functions_Home_Page
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.action_chains import ActionChains
from Functions.General_functions import General_Functions


class loginPage(unittest.TestCase):

    def setUp(self):
        self.driver = webdriver.Chrome(executable_path='C:\Dchroom\R\chromedriver.exe')

    def test_invalidCredentials(self):
        driver = self.driver
        driver.get('https://demo.nopcommerce.com/login')
        driver.maximize_window()
        driver.find_element(By.XPATH,'//*[@class="email"]').send_keys('gabo12312@yopmail.com')
        driver.find_element(By.XPATH,'//*[@class="password"]').send_keys('gabo12312@yopmail.com')
        driver.find_element(By.XPATH,'//*[@id="RememberMe"]').click()
        driver.find_element(By.XPATH,'//button[text()="Log in"]').click()
        if WebDriverWait(driver, 2).until(EC.presence_of_element_located((By.XPATH, '//*[@class="message-error validation-summary-errors"]'))):
            equalText = driver.find_element(By.XPATH, '//*[@class="message-error validation-summary-errors"]').text
            assert equalText == 'Login was unsuccessful. Please correct the errors and try again.\nNo customer account found'


    def test_validCredentials(self):
        driver = self.driver
        G = General_Functions(driver)
        email = G.test_RecordSuccesfull()
        driver.get('https://demo.nopcommerce.com/login')
        driver.maximize_window()
        driver.find_element(By.XPATH,'//*[@class="email"]').send_keys(email)
        driver.find_element(By.XPATH,'//*[@class="password"]').send_keys(email)
        driver.find_element(By.XPATH,'//*[@id="RememberMe"]').click()
        driver.find_element(By.XPATH,'//button[text()="Log in"]').click()
        if WebDriverWait(driver, 7).until(EC.presence_of_element_located((By.XPATH, '//*[@class="ico-logout"]'))):
            equalText = driver.find_element(By.XPATH, '//*[@class="ico-logout"]').text
            assert equalText == 'Log out'



    def test_WithSpaces(self):
        driver = self.driver
        driver.get('https://demo.nopcommerce.com/login')
        driver.maximize_window()
        driver.find_element(By.XPATH,'//*[@class="email"]').send_keys('gabo12312@yopmail.com ')
        driver.find_element(By.XPATH,'//*[@class="password"]').send_keys('gabo12312@yopmail.com ')
        driver.find_element(By.XPATH,'//*[@id="RememberMe"]').click()
        driver.find_element(By.XPATH,'//button[text()="Log in"]').click()
        if WebDriverWait(driver, 2).until(EC.presence_of_element_located((By.XPATH, '//*[@class="message-error validation-summary-errors"]'))):
            equalText = driver.find_element(By.XPATH, '//*[@class="message-error validation-summary-errors"]').text
            print(equalText)
            assert equalText == 'Login was unsuccessful. Please correct the errors and try again.\nNo customer account found'


    def test_SpaceInUsername(self):
        driver = self.driver
        driver.get('https://demo.nopcommerce.com/login')
        driver.maximize_window()
        driver.find_element(By.XPATH,'//*[@class="email"]').send_keys('gabo12312@yopmail.com ')
        driver.find_element(By.XPATH,'//*[@class="password"]').send_keys('gabo12312@yopmail.com')
        driver.find_element(By.XPATH,'//*[@id="RememberMe"]').click()
        driver.find_element(By.XPATH,'//button[text()="Log in"]').click()
        if WebDriverWait(driver, 2).until(EC.presence_of_element_located((By.XPATH, '//*[@class="message-error validation-summary-errors"]'))):
            equalText = driver.find_element(By.XPATH, '//*[@class="message-error validation-summary-errors"]').text
            print(equalText)
            assert equalText == 'Login was unsuccessful. Please correct the errors and try again.\nNo customer account found'


    def test_UnclickRememberMe(self):
        driver = self.driver
        driver.get('https://demo.nopcommerce.com/login')
        driver.maximize_window()
        driver.find_element(By.XPATH,'//*[@class="email"]').send_keys('gabo12312@yopmail.com ')
        driver.find_element(By.XPATH,'//*[@class="password"]').send_keys('gabo12312@yopmail.com')
        acciones = ActionChains(driver)
        acciones.double_click(driver.find_element(By.XPATH,'//*[@id="RememberMe"]')).perform()


    def test_ForgotPassword(self):
        driver = self.driver
        driver.get('https://demo.nopcommerce.com/login')
        driver.maximize_window()
        driver.find_element(By.XPATH,'//*[@class="email"]').send_keys('gabo12312@yopmail.com ')
        driver.find_element(By.XPATH,'//*[@class="password"]').send_keys('gabo12312@yopmail.com')
        driver.find_element(By.XPATH,'//*[@href="/passwordrecovery"]').click()
        time.sleep(2)
        URL = driver.current_url
        assert URL == 'https://demo.nopcommerce.com/passwordrecovery'


    def test_SpaceBeggining(self):
        driver = self.driver
        driver.get('https://demo.nopcommerce.com/login')
        driver.maximize_window()
        driver.find_element(By.XPATH,'//*[@class="email"]').send_keys('   gabo12312@yopmail.com ')
        driver.find_element(By.XPATH,'//*[@class="password"]').send_keys('gabo12312@yopmail.com')
        driver.find_element(By.XPATH,'//*[@id="RememberMe"]').click()
        driver.find_element(By.XPATH,'//button[text()="Log in"]').click()
        if WebDriverWait(driver, 2).until(EC.presence_of_element_located((By.XPATH, '//*[@class="message-error validation-summary-errors"]'))):
            equalText = driver.find_element(By.XPATH, '//*[@class="message-error validation-summary-errors"]').text
            print(equalText)
            assert equalText == 'Login was unsuccessful. Please correct the errors and try again.\nNo customer account found'


    def tearDown(self):
        self.driver.close()

if __name__ == '__main__':
    unittest.main()