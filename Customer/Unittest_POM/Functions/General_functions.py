import unittest
import time
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support import expected_conditions as EC, select
from selenium.webdriver.support.ui import Select
from selenium.webdriver.support.wait import WebDriverWait
from selenium.common.exceptions import TimeoutException
import random


class General_Functions():


    def __init__(self,driver):
        self.driver = driver


    def rule(self,r1,r2,f1,f2,g1,g2):
        driver = self.driver
        act = General_Functions(driver)
        act.Ingresar(r1,f1,g1)
        act.Ingresar(r2,f2,g2)
        driver.maximize_window()


    def login(self,userRut,passwordRut,button,user,password):
        driver = self.driver
        try:
            driver.find_element_by_xpath(userRut).send_keys(user)
            driver.find_element_by_xpath(passwordRut).send_keys(password)
            driver.find_element_by_xpath(button).click()

        except TimeoutException as ec:
            print("Autenticacion fallida "+ ec.msg)

    def Ingresar(self, ruta, xpath1, xpath2):
        driver = self.driver
        driver.get(ruta)
        driver.find_element(By.XPATH, xpath1).click()
        driver.find_element_by_xpath(xpath2).click()


    def Agregar_grupo_muestra(self):
        driver = self.driver


    def Cargar_evidencias(self, page, indice, fecha, textare, btnaction, close):
        driver = self.driver
        # Page
        driver.find_element_by_xpath(page).click()
        time.sleep(2)
        # Indice of page
        driver.find_element_by_xpath(indice).click()
        time.sleep(2)

        driver.find_element_by_xpath(
            '//*[@id="course-step-content"]/div[2]/div/div[1]/item-evidence-requirement').click()
        time.sleep(10)
        driver.find_element_by_xpath(
            '//*[@id="course-step-content"]/div[2]/div/div[2]/app-evidence-list/div/div[1]/button').click()
        time.sleep(10)
        driver.find_element_by_xpath(fecha).send_keys("07/05/2022")
        driver.find_element_by_xpath(textare).send_keys("07/05/2022")

        # Subimos archivo
        # driver.find_element_by_xpath('//*[@id="mat-dialog-0"]/app-evidence-form-modal/div/div/div/form/div/div[4]/button').send_keys("C:\\Users\\GabrielPC\\OneDrive\\Documentos\\QA Automation\\CrediBanco\\files")
        time.sleep(3)
        driver.find_element_by_xpath(btnaction).click()
        time.sleep(10)
        driver.find_element_by_xpath(close).click()


    def Get_in(self, module):
        driver = self.driver
        if(module=="It"):
            time.sleep(4)
            driver.find_element(By.XPATH,'//*[@id="container-1"]/fuse-sidebar/navbar/navbar-vertical-style-1/div[2]/div[1]/fuse-navigation/div/fuse-nav-vertical-group/div[2]/fuse-nav-vertical-collapsable[1]').click()
            time.sleep(2)
            driver.find_element(By.XPATH,'//*[@id="container-1"]/fuse-sidebar/navbar/navbar-vertical-style-1/div[2]/div[1]/fuse-navigation/div/fuse-nav-vertical-group/div[2]/fuse-nav-vertical-collapsable[1]/div/fuse-nav-vertical-item[1]/a/span').click()
            time.sleep(2)
        elif(module == "Station"):
            time.sleep(2)
            driver.find_element(By.XPATH,'//*[@id="container-1"]/fuse-sidebar/navbar/navbar-vertical-style-1/div[2]/div[1]/fuse-navigation/div/fuse-nav-vertical-group/div[2]/fuse-nav-vertical-collapsable[1]').click()
            time.sleep(2)
            driver.find_element(By.XPATH,'//*[@id="container-1"]/fuse-sidebar/navbar/navbar-vertical-style-1/div[2]/div[1]/fuse-navigation/div/fuse-nav-vertical-group/div[2]/fuse-nav-vertical-collapsable[1]/div/fuse-nav-vertical-item[2]/a/span').click()
            time.sleep(2)
        elif(module == "App"):
            time.sleep(2)
            driver.find_element(By.XPATH,'//*[@id="container-1"]/fuse-sidebar/navbar/navbar-vertical-style-1/div[2]/div[1]/fuse-navigation/div/fuse-nav-vertical-group/div[2]/fuse-nav-vertical-collapsable[1]').click()
            time.sleep(2)
            driver.find_element(By.XPATH,'//*[@id="container-1"]/fuse-sidebar/navbar/navbar-vertical-style-1/div[2]/div[1]/fuse-navigation/div/fuse-nav-vertical-group/div[2]/fuse-nav-vertical-collapsable[1]/div/fuse-nav-vertical-item[3]/a/span').click()
            time.sleep(2)
        elif(module == "NotRelationship"):
            time.sleep(2)
            driver.find_element(By.XPATH,'//*[@id="container-1"]/fuse-sidebar/navbar/navbar-vertical-style-1/div[2]/div[1]/fuse-navigation/div/fuse-nav-vertical-group/div[2]/fuse-nav-vertical-collapsable[1]').click()
            time.sleep(2)
            driver.find_element(By.XPATH,'//*[@id="container-1"]/fuse-sidebar/navbar/navbar-vertical-style-1/div[2]/div[1]/fuse-navigation/div/fuse-nav-vertical-group/div[2]/fuse-nav-vertical-collapsable[1]/div/fuse-nav-vertical-item[4]/a/span').click()
            time.sleep(2)


    def insertActive(self,type,ItActives):
        driver = self.driver

        if(type == "it"):
            activeCode = random.randint(1, 9999999999)
            activeCode = 'Active IT ' + str(activeCode)

            for item in ItActives["items"]:
                if (item["name"] == "code"):
                    driver.find_element(By.XPATH,item["value"]).send_keys(activeCode)

                elif(item["name"] == "date"):
                    time.sleep(2)
                    driver.find_element(By.XPATH,item["value"]).send_keys('4/11/2023')
                elif (item["name"] == "select"):
                    act = General_Functions(driver)
                    List = item["value"]
                    act.selectRandom(List[0],List[1],List[2])

                elif (item["name"] == "checkbox"):
                    print(1)

                elif (item["name"] == "ip"):
                    driver.find_element(By.XPATH,item["value"]).send_keys('172.29.12.113')



    def selectRandom(self,cs,sls, condition):
        driver = self.driver
        time.sleep(2)
        if condition == 'checkbox':
            CONTS = driver.find_elements(By.XPATH, sls)
            cont = 0
            for elem in CONTS:
                cont += 1
            randoNum = random.randint(1, cont)
            cont2 = 0
            for elem in CONTS:
                cont2 += 1
                if cont2 == randoNum:
                    elem.click()
                    return cont2

        elif condition == 'Select':
            select_element = Select(driver.find_element(By.XPATH,cs))
            cont = 0
            for option in select_element.options:
                cont += 1
            randoNum = random.randint(1, cont)
            cont = 0
            for elem in select_element.options:
                cont += 1
                if cont == randoNum:
                    elem.click()

        else:
            time.sleep(4)
            driver.find_element(By.XPATH,cs).click()
            CONTS = driver.find_elements(By.XPATH,sls)
            cont = 0
            for sls in CONTS:
                cont += 1

            cont = cont
            randoNum = random.randint(0, cont)
            cont2 = 0

            for sls in CONTS:
                if (cont2 == randoNum):
                    sls.click()
                cont2 += 1

    def selectBTN(self, cs):
        driver = self.driver
        CONTS = driver.find_elements(By.XPATH, '//*[text()=" Siguiente "]')
        cont2 = 0
        for sls in CONTS:
            if (cont2 == cs):
                time.sleep(3)
                sls.click()
            cont2 += 1



    def selectOption(self,cs):
        driver = self.driver
        CONTS = driver.find_elements(By.XPATH, cs)
        rnd = random.randint(0,1)
        cont = 0
        for list in CONTS:
            if cont == rnd:
                list.click()


    def clearData(self, List):
        driver = self.driver
        for list in List:
            driver.find_element(By.XPATH,list).clear()


    def selectListAction(self,sls):
        driver = self.driver
        time.sleep(2)
        CONTS = driver.find_elements(By.XPATH, sls)
        cont = 0
        randoNum = random.randint(0, cont)
        cont2 = 0
        dataActive = []


        for sls in CONTS:
            cont += 1
        cont = cont-1

        table = driver.find_element(By.XPATH, '//*[@class="mat-table cdk-table mat-sort app-table dark-table"]')
        rows = table.find_elements(By.TAG_NAME, "tr")
        data = []
        contRow = 0
        for row in rows:

            # get all the columns of the row
            cols = row.find_elements_by_tag_name("td")
            row_data = []
            for col in cols:
                cell_data = col.text
                if (randoNum == contRow):
                    dataActive.append(cell_data)
                row_data.append(cell_data)
            data.append(row_data)


            contRow += 1



        for ga in CONTS:
            if (cont2 == randoNum):
                time.sleep(2)
                ga.click()
            cont2 += 1

        return row_data

    def ReadData(self, List,ListReaded):
        driver = self.driver

        for listElements in ListReaded:
            if listElements in List:
                continue;

            else:
                x = 10
                print("El valor de x es: " + x)


    def ifExits(self, table):
        driver = self.driver
        table = driver.find_element(By.XPATH, table)
        rows = table.find_elements(By.TAG_NAME, "tr")
        data = []
        contRow = 0
        for row in rows:
            # get all the columns of the row
            cols = row.find_elements_by_tag_name("td")
            row_data = []
            for col in cols:
                cell_data = col.text
                row_data.append(cell_data)
            data.append(row_data)
            contRow += 1
        return data



    def test_RecordSuccesfull(self):
        driver = self.driver
        driver.get('https://demo.nopcommerce.com/register')
        driver.maximize_window()
        G = General_Functions(driver)
        G.selectRandom('n/a','//*[@class="female"  or @class="male"]','checkbox')
        time.sleep(2)
        driver.find_element(By.XPATH,'//*[@name="FirstName"]').send_keys('Gabriel')
        driver.find_element(By.XPATH,'//*[@id="LastName"]').send_keys('Quesada')
        G.selectRandom('//*[@name="DateOfBirthDay"]', 'value', 'Select')
        G.selectRandom('//*[@name="DateOfBirthMonth"]', 'value', 'Select')
        G.selectRandom('//*[@name="DateOfBirthYear"]', 'value', 'Select')
        rd = random.randint(1,1000)
        email = 'GabrielQA'+str(rd)+'@yopmail.com'
        driver.find_element(By.XPATH,'//*[@name="Email"]').send_keys(email)
        G.selectRandom('n/a', '//*[@name="Newsletter" and @type="checkbox"]', 'checkbox')
        driver.find_element(By.XPATH,'//*[@id="Password"]').send_keys(email)
        driver.find_element(By.XPATH,'//*[@id="ConfirmPassword"]').send_keys(email)
        driver.find_element(By.XPATH,'//*[@id="register-button"]').click()
        time.sleep(5)
        driver.find_element(By.XPATH,'//*[text()="Your registration completed"]')
        return email