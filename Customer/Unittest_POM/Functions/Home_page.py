import time
import unittest
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.support import expected_conditions as Ec, select
from selenium.webdriver.support.ui import Select
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.common.action_chains import ActionChains


class Functions_Home_Page():


    def __init__(self,driver):
        self.driver = driver

    def Time(self,timeH):
        t = time.sleep(timeH)
        return t

    def Volver_Header_Sect1(self, xpath,OPT1):
        driver = self.driver
        driver.find_element_by_xpath(xpath).click()
        time.sleep(3)
        driver.find_element_by_xpath(OPT1).click()
        driver.implicitly_wait(5)
        driver.back()

    def Try_except(self,xpath):
        try:
            return xpath.is_enabled()
        except TimeoutException as ex:
            return 'a'+1


    def Selects(self, Var_coin,option):
        driver = self.driver
        try:
            Var_coin = driver.find_element_by_xpath(Var_coin)
            Select = Var_coin.find_elements_by_tag_name(option)
            for opt in Select:
                print('Valores : %s' % opt.get_attribute('text'))
                opt.click()
                driver.implicitly_wait(4)
        except TimeoutException as ex:
            return print('No existe'+ ex.msg)


    def Redirects(self, xpath, element):
            driver = self.driver
            driver.find_element_by_xpath(xpath).click()
            text = driver.find_element_by_xpath(element)
            assert text.text == "Register" or text.text == "Welcome, Please Sign In!" or text.text == "Wishlist" or text.text == "Shopping cart"


    def Search(self, texto, buttonSearch):
        driver = self.driver
        ID = driver.find_element(By.XPATH, texto)
        ID.send_keys('book')
        driver.find_element_by_xpath(buttonSearch).click()
        time.sleep(2)
        btnDism = driver.find_elements(By.XPATH,'//*[@class="product-title"]')
        for ListProd in btnDism:
            print(ListProd.text)
            if('Book' in ListProd.text or 'book' in ListProd.text):
                break
            else:
                1+'a'

    def Hover(self,xpath):
        driver = self.driver
        element_to_hover_over = driver.find_element_by_xpath(xpath)
        hover = ActionChains(driver).move_to_element(element_to_hover_over)
        hover.perform()
