import unittest
import time
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support import expected_conditions as EC, select
from selenium.webdriver.support.ui import Select
from selenium.webdriver.support.wait import WebDriverWait
from selenium.common.exceptions import TimeoutException, NoSuchElementException
import random
from selenium.common.exceptions import NoSuchElementException
from Customer.Unittest_POM.Functions.General_functions import General_Functions


class Buy_forms():


    def __init__(self,driver):
        self.driver = driver

    def rule7(self):
        driver = self.driver
        act = General_Functions(driver)
        try:
            driver.find_element(By.XPATH, '//*[text()="Add to cart"]')
            act.selectRandom('n/a', '//*[text()="Add to cart"]', 'checkbox')
            time.sleep(2)
            driver.find_element(By.XPATH, '//*[text()="The product has been added to your "]')
            print('Add to cart')
        except NoSuchElementException:
            print('WRL')

    def rule6(self):
        driver = self.driver
        act = General_Functions(driver)
        try:
            driver.find_element(By.XPATH, '//*[text()="Your Name:"]')
            driver.find_element(By.XPATH, '//*[@class="recipient-name"]').send_keys('Gabriel')
            driver.find_element(By.XPATH, '//*[@class="sender-name"]').send_keys('GabrielQA')
            driver.find_element(By.XPATH, '//*[@class="message"]').send_keys('N/A')
            act.selectRandom('n/a', '//*[text()="Add to cart"]', 'checkbox')
            time.sleep(2)
            driver.find_element(By.XPATH, '//*[text()="The product has been added to your "]')
            print('Your Name:')
        except NoSuchElementException:
            Buy_forms.rule7(self)

    def rule5(self):
        driver = self.driver
        act = General_Functions(driver)
        try:
            driver.find_element(By.XPATH, '//*[text()="Your Email:"]')
            driver.find_element(By.XPATH, '//*[@class="recipient-email"]')
            driver.find_element(By.XPATH, '//*[@class="recipient-email"]').send_keys(
                'QaGabriel@yopmail.com')
            driver.find_element(By.XPATH, '//*[@class="sender-email"]').send_keys(
                'QaGabriel@yopmail.com')
            driver.find_element(By.XPATH, '//*[@class="recipient-name"]').send_keys('Gabriel')
            driver.find_element(By.XPATH, '//*[@class="sender-name"]').send_keys('GabrielQA')
            driver.find_element(By.XPATH, '//*[@class="message"]').send_keys('N/A')
            act.selectRandom('n/a', '//*[text()="Add to cart"]', 'checkbox')
            time.sleep(2)
            driver.find_element(By.XPATH, '//*[text()="The product has been added to your "]')
            print('Your Email:')
        except NoSuchElementException:
            Buy_forms.rule6(self)

    def rule4(self):
        driver = self.driver
        act = General_Functions(driver)
        try:
            driver.find_element(By.XPATH, '//*[text()=" Start date: "]')
            elements = driver.find_element(By.XPATH, '//*[@class="datepicker hasDatepicker"]')
            cont = 0
            '''for list in elements:
                if (cont == 0):
                    list.send_keys('6/22/2025')
                else:
                    list.send_keys('6/22/2026')
                cont += 1'''
            driver.find_element(By.XPATH,'/html/body/div[6]/div[3]/div/div[2]/div/div/form/div[2]/div[1]/div[2]/div[5]/div[1]/div[2]/input').send_keys('6/22/2024')
            driver.find_element(By.XPATH,'/html/body/div[6]/div[3]/div/div[2]/div/div/form/div[2]/div[1]/div[2]/div[5]/div[2]/div[2]/input').send_keys('6/22/2025')
            driver.find_element(By.XPATH, '//*[text()="Rent"]').click()
            time.sleep(2)
            driver.find_element(By.XPATH, '//*[text()="The product has been added to your "]')
            print(' Start date: ')
        except NoSuchElementException:
            Buy_forms.rule5(self)

    def rule3(self):
        driver = self.driver
        act = General_Functions(driver)
        try:
            driver.find_element(By.XPATH, '//*[text()=" Enter your text: "]')
            driver.find_element(By.XPATH, '//*[@class="textbox"]').send_keys('na')
            act.selectRandom('n/a', '//*[text()="Add to cart"]', 'checkbox')
            time.sleep(2)
            driver.find_element(By.XPATH, '//*[text()="The product has been added to your "]')
            print(' Enter your text: ')
        except NoSuchElementException:
            Buy_forms.rule4(self)

    def rule2(self):
        driver = self.driver
        act = General_Functions(driver)
        try:
            driver.find_element(By.XPATH, '//*[text()=" RAM "]')
            driver.find_element(By.XPATH, '//*[text()=" Processor "]')
            act.selectRandom('n/a',
                             '/html/body/div[6]/div[3]/div/div[2]/div/div/form/div[2]/div[1]/div[2]/div[6]/dl/dd[3]/ul/li/input',
                             'checkbox')
            act.selectRandom('//*[@data-attr="2"]', 'NA', 'Select')
            act.selectRandom('n/a', '//*[text()="Add to cart"]', 'checkbox')
            time.sleep(2)
            driver.find_element(By.XPATH, '//*[text()="The product has been added to your "]')
            print('RAM')
        except NoSuchElementException:
            print('RAM2')
            Buy_forms.rule3(self)


    def rule(self):
        driver = self.driver
        try:
            driver.find_element(By.XPATH, '//*[@data-gtm-form-interact-field-id="0"]')
            act = General_Functions(driver)
            act.selectRandom('//*[@data-gtm-form-interact-field-id="0"]','NA','Select')
            act.selectRandom('n/a', '//*[text()="Add to cart"]', 'checkbox')
            time.sleep(2)
            driver.find_element(By.XPATH, '//*[text()="The product has been added to your "]')
            print('Size')
        except NoSuchElementException:
            print(2312312)
            Buy_forms.rule2(self)

