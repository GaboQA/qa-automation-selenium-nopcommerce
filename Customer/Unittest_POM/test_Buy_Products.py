import time
import unittest
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support import expected_conditions as EC, select
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import TimeoutException
from Functions.Home_page import Functions_Home_Page
from selenium.webdriver.common.action_chains import ActionChains
from Functions.General_functions import General_Functions
from selenium.common.exceptions import NoSuchElementException
from Functions.Buy_forms import Buy_forms
page = 'https://demo.nopcommerce.com/'

class home_page(unittest.TestCase):


    def setUp(self):
        self.driver = webdriver.Chrome(executable_path='C:\Dchroom\R\chromedriver.exe')

    def test_alert_size(self):
        driver = self.driver
        driver.get(page)
        driver.maximize_window()
        G = General_Functions(driver)
        time.sleep(1)
        G.selectRandom('n/a','/html/body/div[6]/div[2]/ul[1]/li','checkbox')
        actual = driver.current_url
        print(actual)
        if actual != "https://demo.nopcommerce.com/books" and actual != "https://demo.nopcommerce.com/digital-downloads" and actual != "https://demo.nopcommerce.com/gift-cards" and actual != "https://demo.nopcommerce.com/jewelry":
            G.selectRandom('n/a', '//*[@class="sub-category-item"]', 'checkbox')
        G.selectRandom('n/a', '//*[@class="product-item"]', 'checkbox')
        Bf = Buy_forms(driver)
        time.sleep(10)
        Bf.rule()

    def test_alertWithout_size(self):
        driver = self.driver
        driver.get(page)
        driver.maximize_window()
        G = General_Functions(driver)
        time.sleep(5)
        G.selectRandom('n/a','/html/body/div[6]/div[2]/ul[1]/li','checkbox')
        time.sleep(1)
        actual = driver.current_url
        print(actual)
        if actual != "https://demo.nopcommerce.com/books" and actual != "https://demo.nopcommerce.com/digital-downloads" and actual != "https://demo.nopcommerce.com/gift-cards" and actual != "https://demo.nopcommerce.com/jewelry":
            G.selectRandom('n/a', '//*[@class="sub-category-item"]', 'checkbox')
        G.selectRandom('n/a','//*[@class="product-item"]','checkbox')
        cls = driver.find_element(By.XPATH,'//*[@aria-label="Enter a quantity"]')
        cls.clear()
        G.selectRandom('n/a','//*[text()="Add to cart"]','checkbox')
        time.sleep(5)
        driver.find_element(By.XPATH,'//*[text()="Quantity should be positive"]')

    def test_deleteProducts(self):
        driver = self.driver
        driver.get(page)
        driver.maximize_window()
        G = General_Functions(driver)
        time.sleep(5)
        sl = G.selectRandom('n/a','/html/body/div[6]/div[2]/ul[1]/li','checkbox')
        ''' Condition if is Digital'''
        if(sl > 2):
            G.selectRandom('n/a', '//*[text()="Add to cart"]', 'checkbox')

        else:
            G.selectRandom('n/a', '//*[@class="sub-category-item"]', 'checkbox')

        '''G.selectRandom('n/a','//*[text()="Add to cart"]','checkbox')'''
        time.sleep(5)
        '''We need indicate validation king of product'''
        driver.find_element(By.XPATH,'//*[text()="The product has been added to your "]')
        time.sleep(5)
        driver.find_element(By.XPATH,'//*[text()="Shopping cart"]').click()
        time.sleep(2)
        products = driver.find_elements(By.XPATH,'//*[@class="remove-btn"]')
        for list in products:
            list.click()
            time.sleep(2)
        driver.find_element(By.XPATH,'//*[@id="updatecart"]').click()
        time.sleep(3)
        content_result = driver.find_element(By.XPATH,'//*[@class="no-data"]').text
        print(content_result)

    def tearDown(self):
        self.driver.close()


if __name__ == '__main__':
    unittest.main()
